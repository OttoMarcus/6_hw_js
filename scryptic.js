function createNewUser(name, family, bday) {
    let newUser = {
        firstName: [name],
        lastName: [family],
        birthday: [bday],
    }

    newUser.getLogin = function () {
        let log = this.firstName.toString().charAt(0);
        return (log + this.lastName).toLowerCase();

    }

    newUser.getAge = function () {
        let today = new Date();
        let birthday = new Date(this.birthday.toString());
        return parseInt((today - birthday) / (1000 * 60 * 60 * 24 * 365));
    }

    newUser.getPassword = function () {
        let nameFirstLetter = this.firstName.toString().charAt(0).toUpperCase();
        let copySurname = this.lastName + '';
        let surnameLowCase = copySurname.toLowerCase();
        let yearOfBirth = this.birthday.toString();
        yearOfBirth = yearOfBirth.slice(6);
        return nameFirstLetter + surnameLowCase + yearOfBirth
    }

    document.getElementById("demo").innerHTML = `First Name:    ${newUser.firstName}`;
    document.getElementById("demo-1").innerHTML = `Last Name:   ${newUser.lastName}`;
    document.getElementById("demo-2").innerHTML = `Birthday:    ${newUser.birthday}`;
    document.getElementById("demo-3").innerHTML = `Login:   ${newUser.getLogin()}`;
    document.getElementById("demo-4").innerHTML = `Age: ${newUser.getAge()}`;
    document.getElementById("demo-5").innerHTML = `Password:    ${newUser.getPassword()}`;
}